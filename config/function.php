<?php
require_once("config/condb.php");


function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 6; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

// debug array funtion
function dump_arr($arr=null){
     $ar = '';
     $ar .= print('<pre>');
     $ar .= print_r($arr);
     $ar = print('</pre>');
        return $ar;
}

// add to db
function add($tbl_name,$data){
  if(empty($data)) return false;
  $mysqli = getConnected();
  while($element = each($data)){
              $insert[$element["key"]]="'".$element["value"]."'";
          }
  $sql = "INSERT INTO `".$tbl_name."` (".implode(",",array_keys($insert)).") VALUES (".implode(",",array_values($insert)).")";
  //echo "INSERT SQL : ".$sql.'<br>';
  //exit;
  $result = $mysqli->query($sql);
  if($result){
    return true;
  }else{
    return false;
  }
}

function add_w_last_id($tbl_name,$data){
  if(empty($data)) return false;
	$conn = getConnected();
  while($element = each($data)){
              $insert[$element["key"]]="'".$element["value"]."'";
          }
  $sql = "INSERT INTO `".$tbl_name."` (".implode(",",array_keys($insert)).") VALUES (".implode(",",array_values($insert)).")";
  //echo "INSERT SQL : ".$sql.'<br>';
  $rs = mysqli_query($conn,$sql);
  $last_id = mysqli_insert_id($conn);
  if($rs){
    return array(true, $last_id);
  }else{
    return false;
  }
}

function check_duplicate($nameTable,$field_select,$val){
  $mysqli = getConnected();
  $sql = "SELECT ".$field_select." FROM ".$nameTable. " WHERE ".$field_select."='".$val."'";
  $result = $mysqli->query($sql);
  if($result->num_rows>0){
    return false;
  }else{
    return true;
  }
}

function getCurlData($url){
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_TIMEOUT, 10);
curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
$curlData = curl_exec($curl);
curl_close($curl);
return $curlData;
}

function DateThai($strDate)
{
  $strYear = date("Y",strtotime($strDate))+543;
  $strMonth= date("n",strtotime($strDate));
  $strDay= date("j",strtotime($strDate));
  $strHour= date("H",strtotime($strDate));
  $strMinute= date("i",strtotime($strDate));
  $strSeconds= date("s",strtotime($strDate));
  $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
  $strMonthThai=$strMonthCut[$strMonth];
  return "$strDay $strMonthThai $strYear";
}

 ?>
