<?php

$arr_type = array('1' => 'Oral', '2' => 'Poster');
$arr_sub_type = array('1' => 'Emerging Infectious Diseases / One Health',
                      '2' => 'Communicable Diseases',
                      '3' => 'Non Communicable Diseases',
                      '4' => 'Injury',
                      '5' => 'Occupational and Environmental Diseases',
                      '6'=>'Outbreak',
                      '7'=>'Disease Control & Prevention System',
                      '8'=>'Information & Data for Disease Control & Surviellance',
                      '9' => 'Risk Communication',);
 ?>
