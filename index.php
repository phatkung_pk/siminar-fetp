<?php
date_default_timezone_set('Asia/Bangkok');
require_once("config/inc.php");
require_once("config/function.php");
require_once("PHPMailer/PHPMailerAutoload.php");

define('SEND_MAIL_HOST', 'smtp.gmail.com');
define('SEND_MAIL_PORT', '587');
//define('SEND_MAIL_ACC', 'nes.boe.moph@gmail.com');
//define('SEND_MAIL_PWD', 'ly,okit[kf;bmpk]');
define('SEND_MAIL_ACC', 'nes.boe.moph@gmail.com');
define('SEND_MAIL_PWD', 'ly,okit[kf;bmpk');


if($_SERVER["REQUEST_METHOD"] == "POST"){
	 $today = date('Y-m-d');
	 //personal Data
	 $email = (isset($_POST['usr_email'])) ? $_POST['usr_email'] : "";
	 $auto_gen_pwd = randomPassword();
	 $fname = (isset($_POST['usr_fname'])) ? $_POST['usr_fname'] : "";
	 $lname = (isset($_POST['usr_lname'])) ? $_POST['usr_lname'] : "";
	 $phone = (isset($_POST['phone_no'])) ? $_POST['phone_no'] : "";
	 $addr = (isset($_POST['usr_addr'])) ? $_POST['usr_addr'] : "";
	 //abstract Data
	 $abs_type = (isset($_POST['abs_type'])) ? $_POST['abs_type'] : "";
	 $abs_subtype = (isset($_POST['abs_subtype'])) ? $_POST['abs_subtype'] : "";
	 $abs_title = (isset($_POST['abs_title'])) ? $_POST['abs_title'] : "";
	 $abs_author = (isset($_POST['abs_author'])) ? $_POST['abs_author'] : "";
	 $abs_aff = (isset($_POST['abs_aff'])) ? $_POST['abs_aff'] : "";
	 $abs_detail = (isset($_POST['abs_detail'])) ? $_POST['abs_detail'] : "";
	 $abs_kw = (isset($_POST['abs_kw'])) ? $_POST['abs_kw'] : "";

	 $recaptcha = (isset($_POST['g-recaptcha-response'])) ? $_POST['g-recaptcha-response'] : "";
/*
	 // reCAPTCHA secret key
   define('SecretKey', '6LfiBRsUAAAAAPnFYB2186EnUygzvzUcdgTgxXzT');
	 $query_params = [
    'secret' => SecretKey,
    'response' => filter_input(INPUT_POST, 'g-recaptcha-response'),
    'remoteip' => $_SERVER['REMOTE_ADDR']
  ];
  $url = 'https://www.google.com/recaptcha/api/siteverify?'.http_build_query($query_params);
  $result = json_decode(file_get_contents($url), true);


		//dump_arr($_POST);
		//exit;

		if(empty($recaptcha)){
			$msg_recaptcha = "ล้มเหลว : กรุณายืนยันตัวตนว่าไม่ใช่โปรแกรมอัตโนมัติ";
			$url_rediectrecaptcha = "<script>alert('".$msg_recaptcha."'); history.back();</script> ";
			echo $url_rediectrecaptcha;
			exit;
		}else{
			$google_url="https://www.google.com/recaptcha/api/siteverify";
			$secret='6LfiBRsUAAAAAPnFYB2186EnUygzvzUcdgTgxXzT';
			$ip=$_SERVER['REMOTE_ADDR'];
			$url=$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
			$res=getCurlData($url);
			$res= json_decode($res, true);
		}



			 $chk_dup = check_duplicate('tbl_user_register','email',$email);

			 if(!$chk_dup){
				 $msg_chk = "ล้มเหลว : email นี้เคยใช้ลงทะเบียนแล้ว";
				 $url_rediectchk = "<script>alert('".$msg_chk."'); history.back();</script> ";
				 echo $url_rediectchk;
				 exit;
			 }
*/
	 $data1 = array('email' => $email,
 									'password' => $auto_gen_pwd,
								  'fname' => $fname,
							    'lname' => $lname,
						      'phone_no' => $phone,
					        'address' => $addr,
									'reg_date' => $today);

	 //insert tbl_user_register
	 $rs1 = add_w_last_id("tbl_user_register",$data1);

	 if($rs1['0']===true){
		 $last_user_id = $rs1['1'];

		 $data2 = array('u_id' => $last_user_id,
		 								'title' => $abs_title,
										'author' => $abs_author,
										'affiliation' => $abs_aff,
	 									'abs_type' => $abs_type,
										'abs_subtype' => $abs_subtype,
										'abs_detail' => $abs_detail,
										'abs_kw' => $abs_kw,
										'approve_status' => 0,
										'add_abs_date' => $today);

			//insert tbl_abstract
			$rs2 = add("tbl_abstract",$data2);

			if($rs2){
				//send mail
				// email.content
				$html = "
				<!doctype html>
				<html lang=\"th\">
				<head>
				<meta charset=\"utf-8\">
				<title>PPE :: Bureau of Epidemiology</title>
				</head>
				<body style=\"font-family:lucida grande,tahoma,verdana,arial,sans-serif;font-size:1em; color:#364d33;\">
				<div style=\"width:640px;margin:0 auto;padding:10px 20px;background:#f2f2f2;\">
					<h1 style=\"font-size:1em;margin:0;padding:0;\">งานสัมมนาระบาดแห่งชาติ ครั้งที่ 23 & The 9th Global Tephinet Conference</h1>
					<div style=\"padding:10px0;\">
						<p style=\"padding:5px 0; font-size:.80em;font-weight:bold;\">สวัสดี<span style=\"padding-left:5px;color:#1a3118;font-weight:normal;\">".$fname." ".$lname."</span></p>
						<p style=\"padding:5px 0; font-size:.80em;font-weight:bold;\">ท่านได้ส่งบทคัดย่อในหัวข้อ <span style=\"padding-left:5px;color:#2196F3;font-weight:bold;\">".$abs_title."</span></p>
						<p style=\"font-size: .80em;padding: 0; line-height: 4px;\">ขณะนี้ทางผู้จัดได้รับบทคัดย่อของท่านแล้ว</p>
						<p style=\"font-size: .80em;padding: 0; line-height: 4px;\">และจะแจ้งผลการคัดเลือกตาม Email ที่ท่านได้ลงทะเบียนไว้</p>
						<p style=\"font-size: .80em;padding: 0; line-height: 4px;\">ระหว่างวันที่ 22-30 มิถุนายน 2560</p>
					</div>
					<div style=\"font-size:.80em;color:#666;\">
						<p>หมายเหตุ: อย่าตอบกลับอีเมลฉบับนี้ ที่อยู่อีเมลนี้ใช้สำหรับส่งเท่านั้น</งะp>
					</div>
				</div>
				</body>
				</html>";
				$sent_to_full_name = $fname." ".$lname;
				define('SEND_TO', $sent_to_full_name);

				//Create a new PHPMailer instance
				$mail = new PHPMailer;
				//Tell PHPMailer to use SMTP
				$mail->isSMTP();
				//Enable SMTP debugging
				// 0 = off (for production use)
				// 1 = client messages
				// 2 = client and server messages
				$mail->SMTPDebug = 0;
				//Ask for HTML-friendly debug output
				$mail->Debugoutput = 'html';
				//Set language utf-8
				$mail->CharSet = 'UTF-8';
				//Set the hostname of the mail server
				$mail->Host = SEND_MAIL_HOST;
				//Set the SMTP port number - likely to be 25, 465 or 587
				$mail->Port = SEND_MAIL_PORT;
				//Set the encryption system to use - ssl (deprecated) or tls
				$mail->SMTPSecure = 'tls';
				//Whether to use SMTP authentication
				$mail->SMTPAuth = true;
				//Username to use for SMTP authentication
				$mail->Username = SEND_MAIL_ACC;
				//Password to use for SMTP authentication
				$mail->Password = SEND_MAIL_PWD;
				//Set who the message is to be sent from
				$mail->setFrom(SEND_MAIL_ACC, 'NES2017');
				//Set who the message is to be sent to
				$mail->addAddress($email, SEND_TO);
				//Set the subject line
				$mail->Subject = 'Auto Reply : ยืนยันการส่งบทคัดย่อ ';
				//Read an HTML message body from an external file, convert referenced images to embedded,
				//convert HTML into a basic plain-text alternative body
				//$mail->msgHTML(file_get_contents('content.html'), dirname(__FILE__));
				$mail->isHTML(true);
				//$mail->msgHTML(file_get_contents('sendRegisMailFrm.html'), dirname(__FILE__));
				$mail->msgHTML($html);

				//send the message, check for errors
				if (!$mail->send()) {
				    //echo "Mailer Error: " . $mail->ErrorInfo;
						$msg_failed = "ล้มเหลว : ระบบไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูลของท่าน";
						$url_rediectchk_failed = "<script>alert('".$msg_failed."'); history.back();</script> ";
						echo $url_rediectchk_failed;
						exit;
				} else {
				    //echo "Message sent!";
						$msg_ok = "สำเร็จ : ระบบได้รับข้อมูลของท่านแล้ว กรุณาตรวจสอบรายชื่อและความถูกต้องในหน้าแสดงรายชื่อผู้ส่งบทคัดย่อ และตรวจสอบ email เพื่อยืนยันการส่งข้อมูล";
						$url_rediectchk_ok = "<script>alert('".$msg_ok."'); window.location='list.php';</script> ";
						echo $url_rediectchk_ok;
				}
			}
		}






//dump_arr($_POST);

}


 ?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<title>Registration</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-default.css">
	<link rel="stylesheet" href="assets/css/footers/footer-v1.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">

	<!-- CSS Page Style -->
	<link rel="stylesheet" href="assets/css/pages/page_log_reg_v1.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/red.css" id="style_color">
	<link rel="stylesheet" href="assets/css/theme-skins/dark.css">
	<script src="ckeditor/ckeditor.js"></script>
	<!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
</head>
</head>

<body class="boxed-layout container">


		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">ลงทะเบียน / Registration</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li class="active">Registration</li>
				</ul>
			</div><!--/container-->
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->


		<!--=== Content Part ===-->
		<div class="container content-sm">

			<form class="reg-page" method="post" id="form-id" action="">
			<div class="row ">
				<div class="col-md-4">

						<div class="headline"><h2> <i class="fa fa-user-circle-o" aria-hidden="true"></i> ข้อมูลบุคคล / Personal Data</h2></div>

						<label>ชื่อ / First Name <span class="color-red">*</span></label>
						<input type="text" name="usr_fname" id="usr_fname" class="form-control margin-bottom-10" required>

						<label>นามสกุล / Last Name <span class="color-red">*</span></label>
						<input type="text" name="usr_lname" id="usr_lname" class="form-control margin-bottom-10" required>

						<label>อีเมล์ / Email Address <span class="color-red">*</span></label>
						<input type="email" name="usr_email" class="form-control margin-bottom-10" required>

						<label>เบอร์ศัพท์ / Phone. <span class="color-red">*</span></label>
						<input type="text" name="phone_no" class="form-control margin-bottom-10" required>

						<label>ที่อยู่ / Address </label>
						<textarea name="usr_addr" class="form-control margin-bottom-10" rows="5" id="usr_addr"></textarea>
				</div>
				<div class="col-md-8">
					<!-- Owl Carousel v2 -->
					<div class="owl-carousel-v2 owl-carousel-style-v1 margin-bottom-50">
						<h3 class="heading-md">Powerd By</h3>
						<div class="owl-slider-v3">
							<div class="item">
								<img class="img-responsive" src="img-logo/logo-9th.png" alt="9th">
							</div>
							<div class="item">
								<img class="img-responsive" src="img-logo/logo-23th.png" alt="23th">
							</div>
							<div class="item">
								<img class="img-responsive" src="img-logo/logo-MOPH.png" alt="MOPH">
							</div>
							<div class="item">
								<img class="img-responsive" src="img-logo/TEPHINET-20th-logo-signature.png" alt="TEPHINET-20th">
							</div>
							<!--<div class="item">
								<img class="img-responsive" src="assets/img/clients2/bellfield.png" alt="">
							</div>
							<div class="item">
								<img class="img-responsive" src="assets/img/clients2/national-geographic.png" alt="">
							</div>
							<div class="item">
								<img class="img-responsive" src="assets/img/clients2/emirates.png" alt="">
							</div>
							<div class="item">
								<img class="img-responsive" src="assets/img/clients2/co-wheels.png" alt="">
							</div>-->
						</div>

						<!--<div class="owl-navigation">
							<div class="customNavigation">
								<a class="owl-btn prev-v2"><i class="fa fa-angle-left"></i></a>
								<a class="owl-btn next-v2"><i class="fa fa-angle-right"></i></a>
							</div>
						</div>--><!--/navigation-->
					</div>
					<!-- End Owl Carousel v2 -->

				</div>
			</div>
			<hr>
			<div class="row margin-top-20 ">
					<div class="row">
						<div class="col-md-12">

								<div class="headline"><h2> <i class="fa fa-file-text" aria-hidden="true"></i>บทคัดย่อ / Abstract</h2></div>

								<label>ประเภท / Abstract Type <span class="color-red">*</span></label>
								<div class="radio">
								  <label><input type="radio" name="abs_type" checked value="1">Oral</label>
								</div>
								<div class="radio">
								  <label><input type="radio" name="abs_type" value="2">Poster</label>
								</div>

								<label class="select">ประเภทย่อย / Abstract Sub Type <span class="color-red">*</span></label>
								<select name="abs_subtype" class="form-control" style="width:350px" id="sel1">
									<?php foreach ($arr_sub_type as $key => $val) : ?>
									<option value="<?php echo $key;?>"><?php echo $val;?></option>
								<?php endforeach; unset($arr_sub_type);?>
					      </select>
								<br>

								<label>ชื่อเรื่อง / Title <span class="color-red">*</span></label>
								<input type="text" name="abs_title" class="form-control margin-bottom-10" required>

								<label>ชื่อผู้เขียน / Authors' Name </label><span class="color-red">* กรณีผู้เขียนมากกว่า 1 ท่านให้ใส่เครื่องหมาย , (comma) คั่น</span>
								<input type="text" name="abs_author" id="abs_author" class="form-control margin-bottom-10" required>

								<label>สังกัด / Affiliation <span class="color-red">*</span></label>
								<input type="text" name="abs_aff" style="width:450px" class="form-control margin-bottom-10" required>

								<label>เนื้อหา / Content  โดยเรียงจาก บทนำ(Background),วิธีการศึกษา(Methods),ผลการศึกษา(Results),สรุปและวิจารณ์(Conclusion)<span class="color-red">*</span></label>
								<textarea name="abs_detail" id="editor1" rows="10" cols="80" required>

		            </textarea>
		            <script>
		                // Replace the <textarea id="editor1"> with a CKEditor
		                // instance, using default configuration.
		                //CKEDITOR.replace( 'editor1' );
										CKEDITOR.replace( 'editor1', {
    									customConfig: 'config.js'
										});
		            </script>
									<br>
								<label>คำสำคัญ / Keywords <span class="color-red">*</span></label>
								<input type="text" name="abs_kw" style="width:450px" class="form-control margin-bottom-10" required>

								<!--<div class="g-recaptcha pull-right" data-sitekey="6LfiBRsUAAAAAH3fTe5k85acWawF953s-5vmOL7p"></div>-->
						</div>
				</div>

						<div class="row pull-right margin-top-20">
							<div class="col-md-12">
								<label>
									<input id="mycheckbox" type="checkbox">
									ฉันได้อ่าน <a href="#" class="color-red" data-toggle="modal" data-target=".bs-example-modal-lg">เงื่อนไขและข้อตกลง</a> แล้ว / I read <a href="#" class="color-red" data-toggle="modal" data-target=".bs-example-modal-lg">Terms and Conditions</a>
								</label>


								<button class="btn-u" id="submit" type="submit">Register</button>

								<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-header">
												<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
												<h4 id="myLargeModalLabel2" class="modal-title">เงื่อนไขและข้อตกลง / Terms and Conditions</h4>
											</div>
											<div class="modal-body">
												<!-- <p>1.ผู้ส่งบทคัดย่อจะต้องมีผลทดสอบภาษาอังกฤษจากกระทรวงต่างประเทศ <strong class="color-red">เกณฑ์ผ่าน 35 คะแนน หรือจบการศึกษาจากต่างประเทศ</strong></p>-->
												<p>1.ผู้ส่งบทคัดย่อ <strong class="color-red">ยินยอมให้เผยแพร่บทความทางวิชาการได้</strong></p>
											</div>
											<div class="modal-footer">
											<button type="button" class="btn-u" data-dismiss="modal">ปิด / Close</button>
										</div>
										</div>

									</div>

								</div>
								<!-- Large modal -->
							</div>
						</div>
			</div>
		</form>
		</div><!--/container-->
		<!--=== End Content Part ===-->

		<!--=== Footer Version 1 ===-->
		<div class="footer-v1">
			<div class="footer1">
				<div class="container">

				</div>
			</div><!--/footer-->

			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p>
								2017 &copy; All Rights Reserved.
								<a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
							</p>
						</div>
						<!-- Social Links -->
						<!--
						<div class="col-md-6">
							<ul class="footer-socials list-inline">
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
										<i class="fa fa-facebook"></i>
									</a>
								</li>
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Skype">
										<i class="fa fa-skype"></i>
									</a>
								</li>
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
										<i class="fa fa-google-plus"></i>
									</a>
								</li>
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin">
										<i class="fa fa-linkedin"></i>
									</a>
								</li>
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest">
										<i class="fa fa-pinterest"></i>
									</a>
								</li>
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
										<i class="fa fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dribbble">
										<i class="fa fa-dribbble"></i>
									</a>
								</li>
							</ul>
						</div>
					-->
						<!-- End Social Links -->
					</div>
				</div>
			</div><!--/copyright-->
		</div>
		<!--=== End Footer Version 1 ===-->
	</div>

	<!-- JS Global Compulsory -->
	<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
	<script type="text/javascript" src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="assets/js/custom.js"></script>
	<!-- JS Page Level -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script src="assets/js/plugins/owl-carousel.js"></script>



	<script type="text/javascript">
		jQuery(document).ready(function() {

			App.init();
			OwlCarousel.initOwlCarousel();

			var $form = $('#form-id');
	    var $checkbox = $('#mycheckbox');

			/*
			$("#usr_fname").keyup(function(){
						  var txt_fname = $("#usr_fname").val();
							$("#usr_lname").keyup(function(){
											var txt_lname = $('#usr_lname').val();
											var get_full_name = txt_fname+' '+txt_lname;
										  $('#abs_author').val(get_full_name);
				        });
        });
				*/

	    $form.on('submit', function(e) {
	        if(!$checkbox.is(':checked')) {
	            alert('กรุณาอ่านเงื่อนไขและข้อตกลง / Please Read Terms and Conditions!');
	            e.preventDefault();
	        }
	    });
		});
	</script>
	<script type="text/javascript">
		$.backstretch([
			"assets/img/bg/7.jpg"
			], {
				fade: 1000
			});
	</script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/respond.js"></script>
	<script src="assets/plugins/html5shiv.js"></script>
	<script src="assets/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->
</body>
</html>
