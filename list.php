<?php
require_once("config/inc.php");
require_once("config/condb.php");
require_once("config/function.php");
// local
$conn_db = getConnected();
// www
//$mysqli = new mysqli("localhost","prosecure_proj","Adm1n24prosecure","prosecure_proj"); //Connect DB



 ?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<title>Registered</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-default.css">
	<link rel="stylesheet" href="assets/css/footers/footer-v1.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">

	<!-- CSS Page Style -->
	<link rel="stylesheet" href="assets/css/pages/page_log_reg_v1.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/red.css" id="style_color">
	<link rel="stylesheet" href="assets/css/theme-skins/dark.css">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">

</head>
</head>

<body class="boxed-layout container">


		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">รายชื่อผู้ส่งบทคัดย่อ / Registered</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li class="active">Registered</li>
				</ul>
			</div><!--/container-->
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->


		<!--=== Content Part ===-->
		<div class="container content-sm">
			<div class="margin-top-20"></div>
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
             <div class="col-xs-4">
             </div>
             <div class="col-xs-4">
                 <button type="button" onclick="window.location.href = 'index.php'" class="btn btn-success btn-lg text-center"><i class="fa fa-user-plus" aria-hidden="true"></i> ลงทะเบียนส่งบทคัดย่อที่นี่</button>
             </div>
             <div class="col-xs-4">
             </div>
         </div>
        </div>
      </div>
      <div class="margin-top-20"></div>
					<div class="table-responsive">
						<table id="myTable" class="display" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>ชื่อ-นามสกุล</th>
			                <th>ชื่อเรื่อง</th>
			                <th>ประเภท</th>
			                <th>ประเภทย่อย</th>
			                <th>วันที่ส่ง</th>
			            </tr>
			        </thead>
			        <tfoot>
			            <tr>
										<th>ชื่อ-นามสกุล</th>
										<th>ชื่อเรื่อง</th>
										<th>ประเภท</th>
										<th>ประเภทย่อย</th>
										<th>วันที่ส่ง</th>
			            </tr>
			        </tfoot>
			        <tbody>
							<?php
							 $sql = "SELECT tbl_user_register.fname as fname ,tbl_user_register.lname as lname ,tbl_abstract.title as title ,tbl_abstract.abs_type as type ,tbl_abstract.abs_subtype as sub_type,tbl_user_register.reg_date as reg_date ";
							 $sql.= " FROM tbl_abstract LEFT JOIN tbl_user_register ON tbl_abstract.u_id = tbl_user_register.u_id";
							 $sql.= " GROUP BY tbl_user_register.fname,tbl_user_register.lname,tbl_abstract.title,tbl_abstract.abs_type,tbl_abstract.abs_subtype,tbl_user_register.reg_date order by tbl_abstract.abs_id desc";
							 $result_select = $conn_db->query($sql);

			         $num = $result_select->num_rows;
							 //echo $sql;
							 //check row to display
							 if($num>0) {
			         while($result = $result_select->fetch_assoc()) { ?>
			            <tr>
			                <td><?php echo trim($result["fname"]).' '.trim($result["lname"]);?></td>
			                <td><font size="2px"><?php echo $result["title"];?></font></td>
			                <td><?php echo $arr_type[$result["type"]];?></td>
			                <td><?php echo $arr_sub_type[$result["sub_type"]];?></td>
			                <td><?php echo DateThai($result["reg_date"]);?></td>
			            </tr>
							<?php } // end while
						   } // end if num>0
							 else{ ?>
								 <tr>
										 <td colspan="5" align="center">ไม่พบข้อมูล</td>
								 </tr>
							<?php } // end if?>
			        </tbody>
			    </table>
			</div> <!-- end responsive table-->
      <div class="margin-top-20"></div>
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
             <div class="col-xs-4">
             </div>
             <div class="col-xs-4">
                 <button type="button" onclick="window.location.href = 'index.php'" class="btn btn-success btn-lg text-center"><i class="fa fa-user-plus" aria-hidden="true"></i> ลงทะเบียนส่งบทคัดย่อที่นี่</button>
             </div>
             <div class="col-xs-4">
             </div>
         </div>
        </div>
      </div>
		</div><!--/container-->
		<!--=== End Content Part ===-->

		<!--=== Footer Version 1 ===-->
		<div class="footer-v1">
			<div class="footer1">
				<div class="container">

				</div>
			</div><!--/footer-->

			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p>
								2017 &copy; All Rights Reserved.
								<a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
							</p>
						</div>

					</div>
				</div>
			</div><!--/copyright-->
		</div>
		<!--=== End Footer Version 1 ===-->
	</div>

	<!-- JS Global Compulsory -->
	<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
	<script type="text/javascript" src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="assets/js/custom.js"></script>
	<!-- JS Page Level -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script src="assets/js/plugins/owl-carousel.js"></script>



	<script type="text/javascript">
		jQuery(document).ready(function() {

			App.init();
			OwlCarousel.initOwlCarousel();
		});
	</script>
	<script type="text/javascript">
		$.backstretch([
			"assets/img/bg/7.jpg"
			], {
				fade: 1000
			});
	</script>
	<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
			$(document).ready(function(){
		    $('#myTable').DataTable({
					"order": [[ 4, 'desc' ]],
					"scrollX": true,
					"oLanguage": {
						"sProcessing":   "กำลังดำเนินการ...",
						"sLengthMenu":   "แสดง _MENU_ แถว",
						"sZeroRecords":  "ไม่พบข้อมูล",
						"sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
						"sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
						"sInfoFiltered": "(กรองข้อมูลจากทั้งหมด _MAX_ แถว)",
						"sInfoPostFix":  "",
						"sSearch":       "ค้นหา:",
						"sUrl":          "",
						"oPaginate": {
								"sFirst":    "เิริ่มต้น",
								"sPrevious": "ก่อนหน้า",
								"sNext":     "ถัดไป",
								"sLast":     "สุดท้าย"
						}
					}
				});
		  });
	</script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/respond.js"></script>
	<script src="assets/plugins/html5shiv.js"></script>
	<script src="assets/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->
</body>
</html>
<?php $conn_db->close();
unset($arr_type);
unset($arr_sub_type);
?>
