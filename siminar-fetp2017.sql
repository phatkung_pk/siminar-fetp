-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: mysql:3306
-- Generation Time: Mar 30, 2017 at 03:42 AM
-- Server version: 5.7.16
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siminar-fetp2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_abstract`
--

CREATE TABLE `tbl_abstract` (
  `abs_id` int(10) NOT NULL,
  `u_id` int(10) NOT NULL,
  `title` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `affiliation` varchar(255) NOT NULL,
  `abs_type` int(2) NOT NULL,
  `abs_subtype` int(2) NOT NULL,
  `abs_detail` text,
  `approve_status` varchar(1) NOT NULL,
  `add_abs_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_abstract`
--

INSERT INTO `tbl_abstract` (`abs_id`, `u_id`, `title`, `author`, `affiliation`, `abs_type`, `abs_subtype`, `abs_detail`, `approve_status`, `add_abs_date`) VALUES
(1, 1, 'fdsfsd', 'dsdakldkdlskal;da fdfsdfds', 'fdsfds', 1, 1, '<p>fdfsdfdfsfds fjdksfjkldsfjklfjkdsljfdsfjklsdfjkdfjksfjdlsfl;</p>\r\n', '0', '2017-03-29'),
(2, 2, 'fsdfdsfdsfsd', 'dssadsdasda dsdasadsa', 'fdsfdsfdsfds', 1, 1, '<p>fdsfdsfsdfsds dsk,fdskfl;dsfldsklfdkf; ,fdsl;f,s;fk;lkfl;kfdsfds</p>\r\n', '0', '2017-03-29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_register`
--

CREATE TABLE `tbl_user_register` (
  `u_id` int(11) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL,
  `fname` varchar(100) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `phone_no` varchar(50) DEFAULT NULL,
  `address` text,
  `reg_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user_register`
--

INSERT INTO `tbl_user_register` (`u_id`, `email`, `password`, `fname`, `lname`, `phone_no`, `address`, `reg_date`) VALUES
(1, 'and1_phat@hotmail.com', 'b4ot6k', 'dsdakldkdlskal;da', 'fdfsdfds', 'fdsfs', 'fdsfds', '2017-03-29'),
(2, 'phatkung.kizz@gmail.com', 'He47Wv', 'dssadsdasda', 'dsdasadsa', '55555', 'fdfsfdsf', '2017-03-29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_abstract`
--
ALTER TABLE `tbl_abstract`
  ADD PRIMARY KEY (`abs_id`);

--
-- Indexes for table `tbl_user_register`
--
ALTER TABLE `tbl_user_register`
  ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_abstract`
--
ALTER TABLE `tbl_abstract`
  MODIFY `abs_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_user_register`
--
ALTER TABLE `tbl_user_register`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
